<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <link rel="stylesheet" href="/public/css/normalize.css" />
    <link rel="stylesheet" href="/public/css/style.css" />
    <link rel="stylesheet" href="/public/css/font-awesome.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="header">
            <div class="row">
                <div class="col-md-12">algemene meldingen en foutmeldingen:</div>
            </div>
            <?php if (count($messages) > 0 || count($errors) > 0) { ?>
            <div class="row">
                <div class="col-md-6">
                    <h2>Foutmeldingen</h2>
                    <ul class="errors">
                    <?php foreach ($errors as $error) { ?>
                        <li><?php print $error; ?></li>
                    <?php } ?>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h2>Meldingen</h2>
                    <ul class="messages">
                    <?php foreach ($messages as $message) { ?>
                        <li><?php print $message; ?></li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } ?>
        </div>