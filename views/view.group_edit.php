<?php include("view.header.php"); ?>
<div class="contents">
    <div class="row">
	<div class="col-md-12">
            <h1>Groep "<?php print $group["name"]; ?>"</h1>
	</div>
    </div>
    
    <div class="row toolbar">
        <div class="col-md-2"></div>
        <div class="col-md-4"><a href="/"><i class="fa fa-home"></i> Dashboard</a></div>
        <div class="col-md-2"><a href="/group/new"><i class="fa fa-plus"></i> Nieuw item</a></div>
        <div class="col-md-2"><a href="/group/new"><i class="fa fa-plus"></i> Nieuwe groep</a></div>
        <div class="col-md-2"></div>
    </div>
    
    <div class="form">
    <form name="item_new" method="post" action="/group/save">
        <input type="hidden" name="id" value="<?php print $group["id"]; ?>"/>
        <div class="row">
            <div class="col-md-4">Naam:</div>
            <div class="col-md-8"><input type="input" type="text" name="name" value="<?php print $group["name"]; ?>"/></div>
        </div>
        <div class="row">
            <div class="col-md-4">Groep:</div>
            <div class="col-md-8">
                <select name="parent" autocomplete="off">
                    <option value="">-geen groep-</option>
                    <?php print GroupController::HTMLGroupOptions($groups, $group["parent"], $group["id"]); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Volgorde:</div>
            <div class="col-md-8">
                <select name="position" autocomplete="off">
                    <?php foreach ($siblings as $index => $sibling) { ?>
                        <?php if ($sibling["id"] == $group["id"]) { ?>
                            <option value="<?php print $index; ?>" selected="selected">Huidige positie</option>
                        <?php } else { ?>
                            <option value="<?php print $index; ?>">Voor <?php print $sibling["name"] . " (" . $sibling["position"] . ")"; ?></option>
                        <?php } ?>
                    <?php } ?>
                    <option value="<?php print count($siblings); ?>">-achteraan-</option>
                </select>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary" value="1">Opslaan</button> 
            </div>
        </div>
    </form>
    </div>
</div>
<?php include("view.footer.php"); ?>