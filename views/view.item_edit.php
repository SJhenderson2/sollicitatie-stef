<?php include("view.header.php"); ?>
<div class="contents">
    <div class="row">
	<div class="col-md-12">
            <h1>Item '<?php print $item["name"]; ?>' aanpassen</h1>
	</div>
    </div>
    
    <div class="row toolbar">
        <div class="col-md-2"></div>
        <div class="col-md-4"><a href="/"><i class="fa fa-home"></i> Dashboard</a></div>
        <div class="col-md-2"><a href="/group/create"><i class="fa fa-plus"></i> Nieuwe groep</a></div>
        <div class="col-md-2"><a href="/item/create"><i class="fa fa-coffee"></i> Nieuw item</a></div>
        <div class="col-md-2"></div>
    </div>
    
    <div class="form">
    <form name="item_new" method="post" action="/item/save">
        <input type="hidden" name="id" value="<?php print $item["id"]; ?>" />
        <div class="row">
            <div class="col-md-4">Naam:</div>
            <div class="col-md-8"><input type="input" type="text" name="name" value="<?php print $item["name"]; ?>" autocomplete="off" /></div>
        </div>
        <div class="row">
            <div class="col-md-4">Groep:</div>
            <div class="col-md-8">
                <select name="parent" autocomplete="off">
                    <option value="">-geen groep-</option>
                    <?php print ItemController::HTMLGroupOptions($groups, $item["parent"]); ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                Items worden altijd achteraan geplaats bij een groepswijziging.
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Volgorde:</div>
            <div class="col-md-8">
                <select name="position" autocomplete="off">
                    <?php foreach ($siblings as $index => $sibling) { ?>
                        <?php if ($sibling["id"] == $item["id"]) { ?>
                            <option value="<?php print $index; ?>" selected="selected">Huidige positie</option>
                        <?php } else { ?>
                            <option value="<?php print $index; ?>">Voor <?php print $sibling["name"] . " (" . $sibling["position"] . ")"; ?></option>
                        <?php } ?>
                    <?php } ?>
                    <option value="<?php print count($siblings); ?>">-achteraan-</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary" value="1">Opslaan</button> 
            </div>
        </div>
    </form>
    </div>
</div>
<?php include("view.footer.php"); ?>