<?php include("view.header.php"); ?>
<div class="contents">
    <div class="row">
	<div class="col-md-12">
            <h1>Nieuw item</h1>
	</div>
    </div>
    
    <div class="row toolbar">
        <div class="col-md-2"></div>
        <div class="col-md-4"><a href="/"><i class="fa fa-home"></i> Dashboard</a></div>
        <div class="col-md-4"><a href="/group/create"><i class="fa fa-plus"></i> Nieuwe groep</a></div>
        <div class="col-md-2"></div>
    </div>
    
    <div class="form">
    <form name="item_new" method="post" action="/item/save">
        <div class="row">
            <div class="col-md-4">Naam:</div>
            <div class="col-md-8"><input type="input" type="text" name="name" value="" /></div>
        </div>
        <div class="row">
            <div class="col-md-4">Groep:</div>
            <div class="col-md-8">
            <?php if (!empty($groupId) ) { ?>
                <input type="input" type="hidden" name="parent" value="" />
                <?php print $group["name"]; ?>
            <?php } else { ?>
                <select name="parent">
                    <option value="">-geen groep-</option>
                    <?php print ItemController::HTMLGroupOptions($groups); ?>
                </select>
            <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Volgorde:</div>
            <div class="col-md-8">
                <select name="position">
                    <option value="">-achteraan-</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary" value="1">Opslaan</button> 
            </div>
        </div>
    </form>
    </div>
</div>
<?php include("view.footer.php"); ?>