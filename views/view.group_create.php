<?php include("view.header.php"); ?>
<div class="contents">
    <div class="row">
	<div class="col-md-12">
            <h1>Nieuwe groep</h1>
	</div>
    </div>
    
    <div class="row toolbar">
        <div class="col-md-2"></div>
        <div class="col-md-4"><a href="/"><i class="fa fa-home"></i> Dashboard</a></div>
        <div class="col-md-4"><a href="/group/new"><i class="fa fa-plus"></i> Nieuw item</a></div>
        <div class="col-md-2"></div>
    </div>
    
    <div class="form">
    <form name="item_new" method="post" action="/group/save">
        <div class="row">
            <div class="col-md-4">Naam:</div>
            <div class="col-md-8"><input type="input" type="text" name="name" /></div>
        </div>
        <div class="row">
            <div class="col-md-4">Groep:</div>
            <div class="col-md-8">
            <?php if (!empty($groupId) ) { ?>
                <input type="input" type="hidden" name="parent" value="" />
                <?php print $group["name"]; ?>
            <?php } else { ?>
                <select name="parent">
                    <option value="">-geen groep-</option>
                    <?php print GroupController::HTMLGroupOptions($groups, $group["parent"]); ?>
                </select>
            <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Volgorde:</div>
            <div class="col-md-8">-achteraan-</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary" value="1">Opslaan</button> 
            </div>
        </div>
    </form>
    </div>
</div>
<?php include("view.footer.php"); ?>