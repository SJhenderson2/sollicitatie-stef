<?php include("view.header.php"); ?>
<div class="contents">
    <div class="row">
	<div class="col-md-12">
            <h1>Dashboard</h1>
	</div>
    </div>
    
    <div class="row toolbar">
        <div class="col-md-6"><a href="/group/create"><i class="fa fa-plus"></i> Nieuwe groep</a></div>
        <div class="col-md-6"><a href="/item/create"><i class="fa fa-plus"></i> Nieuw item</a></div>
    </div>
    
    <?php print IndexController::HTMLGroupDisplay($groups); ?>
    
    <?php foreach ($items as $item) { ?>
        <div class="row">
            <div class="col-md-8"><div class="leveler" style="width: <?php print ($item["level"]+1)*20; ?>px;"></div> <a href="/item/edit?id=<?php print $item["id"]; ?>"><i class="fa fa-coffee"></i> <?php print ($item["position"]+1) . " " . $item["name"]; ?></a></div>
        </div>
    <?php } ?>
</div>
<?php include("view.footer.php"); ?>