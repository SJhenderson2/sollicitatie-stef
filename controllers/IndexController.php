<?php
class IndexController extends Controller {
    public function index() {
        $html = "";

        list($items, $groups) = ItemModule::fetchAll();
        
        $params = [
            "items" => $items,
            "groups" => $groups
        ];

        $html = self::view("dashboard", $params);
        if ($html === false) {
            $html = self::error();
        }
        return $html;
    }
    
    public static function HTMLGroupDisplay($groups, $level=0) {
        $html = "";
        foreach ($groups as $group) {
            $html .= self::view("dashboard_group", $group);
            if (!empty($group["children"]) ) {
                $html .= self::HTMLGroupDisplay($group["children"], ($level+1));
            }
            if (!empty($group["items"]) ) {
                foreach ($group["items"] as $item) {
                    $html .= self::view("dashboard_item", $item);
                }
            }
        }
        return $html;
    }
}

?>