<?php
class ItemController extends Controller {
    public function actionSave() {
        if (!empty($_POST) ) {
            $id = (!empty($_POST["id"]) ? $_POST["id"] : null);
            $name = (!empty($_POST["name"]) ? $_POST["name"] : null);
            $parent = (!empty($_POST["parent"]) ? $_POST["parent"] : null);
            $position = (array_key_exists("position", $_POST) ? $_POST["position"] : null);
            $errors = [];
            if (empty($name) ) {
                $errors[] = "Een naam is verplicht";
            }
            if (empty($errors)) {
                if (empty($id) ) {
                    if (ItemModule::insertItem($name, $parent) ) {
                        $url = "/?messages[]=". urlencode("Item is toegevoegd");
                    } else {
                        $url = "/?errors[]=". urlencode("Item is niet toegevoegd");
                    }
                    header("location: {$url}");
                } else {
                    if (ItemModule::updateItem($id, $name, $parent, $position) ) {
                        $url = "/?messages[]=". urlencode("Item is aangepast");
                    } else {
                        die();
                        $url = "/?errors[]=". urlencode("Item is niet aangepast");
                    }
                    header("location: {$url}");
                }
            } else {
                if (empty($id) ) {
                    $url = "/item/create?1=1";
                    foreach ($errors as $error) {
                        $url .= "&errors[]=" . urlencode($error);
                    }
                } else {
                    $url = "/item/edit?id=" . $id;
                    foreach ($errors as $error) {
                        $url .= "&errors[]=" . urlencode($error);
                    }
                }
                header("location: {$url}");
            }
        }
    }
    
    public function actionCreate() {
        $groupId = (!empty($_GET["group_id"]) ? $_GET["group_id"] : null);
        $group = null;
        $groups = [];
        
        if (!empty($groupId) ) {
            $group = GroupModule::fetch($groupId);
            if (empty($group) ) {
                $groupId = null;
            }
        }
        if (empty($groupId) ) {
            $groups = GroupModule::fetchAll();
        }
        
        if (empty($groups) && empty($groupId) ) {
            Container::$errors[] = "Item creation not possible; create group first";
            return self::error();
        }
        
        $params = [
            "groupId" => $groupId,
            "group" => $group,
            "groups" => $groups
        ];
        
        $html = self::view("item_create", $params);
        if ($html === false) {
            return self::error();
        }
        return $html;
    }
    
    public function actionEdit() {
        $id = (!empty($_GET["id"]) ? $_GET["id"] : null);
        
        $item = ItemModule::fetchItem($id);
        if ($item === false) {
            Container::$errors[] = "actionEdit error";
            return self::error();
        }
        $groups = [];
        
        $groups = GroupModule::fetchAll();
        
        $siblings = ItemModule::fetchItems($item["parent"]);
        
        $params = [
            "item" => $item,
            "siblings" => $siblings,
            "groups" => $groups
        ];
        
        $html = self::view("item_edit", $params);
        if ($html === false) {
            return self::error();
        }
        return $html;
    }
    
    public static function HTMLGroupOptions($groups, $parent=null, $level=0) {
        $html = "";
        foreach ($groups as $group) {
            $name = "";
            for ($n=0; $n<$level; $n++) {
                $name .= ".";
            }
            $name .= $group["name"];
            $html .= '<option value="' . $group["id"] . '"';
            if ($group["id"] == $parent) {
                $html .= ' selected="selected"';
            }
            $html .= '>' . $name . '</option>';
            if (!empty($group["children"]) ) {
                $html .= self::HTMLGroupOptions($group["children"], $parent, ($level+1));
            }
        }
        return $html;
    }
}
?>