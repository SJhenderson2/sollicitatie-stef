<?php
// Loading classes 
require("classes/Container.php");
require("classes/Controller.php");
require("classes/Module.php");
require("classes/ItemModule.php");
require("classes/GroupModule.php");

$urlParts = [];

if (!empty($_SERVER["REQUEST_URI"]) ) {
    $qspos = strpos($_SERVER["REQUEST_URI"], "?");
    if ($qspos !== false) {
        $requestUri = substr($_SERVER["REQUEST_URI"], 0, $qspos);
    } else {
        $requestUri = $_SERVER["REQUEST_URI"];
    }
    $urlParts = explode("/", $requestUri);
    array_shift($urlParts); // remove empty
}

Module::init();
Container::init();

$controller = null;
$action = null;

if (count($urlParts) >= 1 && !empty($urlParts[0]) ) { // page / module
    $controllerClass = ucfirst(strtolower($urlParts[0]) ) . "Controller";
    if (file_exists("controllers/{$controllerClass}.php") ) {
        require("controllers/{$controllerClass}.php");
        $controller = new $controllerClass();

        if (count($urlParts) >= 2) { // action
            $action = strtolower($urlParts[1]);
        }
    } else {
        Container::$errors[] = "Controller {$controllerClass} not found";
    }
}

if (is_null($controller) ) {
    require("controllers/IndexController.php");
    $controller = new IndexController();
}

if (!empty($action) ) {
    if (method_exists($controller, "action" . ucfirst($action)) ) {
        $actionName = "action" . ucfirst($action);
        $output = $controller->$actionName();
    } else {
        Container::$errors[] = "Action {$action} not found";
        $output = $controller->error();
    }
} else {
    $output = $controller->index();
}

print $output;
?>