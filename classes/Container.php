<?php

class Container {
    public static $messages = [];
    public static $errors = [];
    
    public static function init() {
        $errors = [];
        $messages = [];
        if (!empty($_GET["errors"]) ) {
            if (is_array($_GET["errors"]) ) {
                $errors = $_GET["errors"];
            } else {
                $errors = array($_GET["errors"]);
            }
            self::$errors = $errors;
        }
        if (!empty($_GET["messages"]) ) {
            if (is_array($_GET["messages"]) ) {
                $messages = $_GET["messages"];
            } else {
                $messages = array($_GET["messages"]);
            }
            self::$messages = $messages;
        }
    }
}