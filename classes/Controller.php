<?php

class Controller {
	
    protected function view($file, $params=[]) {
        $path = "views/view.{$file}.php";

        if (!array_key_exists("messages", $params) ) {
            $params["messages"] = Container::$messages;
        }
        if (!array_key_exists("errors", $params) ) {
            $params["errors"] = Container::$errors;
        }

        if (file_exists($path) ) {
            foreach ($params as $key => $value) {
                ${$key} = $value;
            }
            ob_start();
            include($path);
            $contents = ob_get_contents();
            ob_end_clean();

            return $contents;
        } else {
            Container::$errors[] = "View {$file} not found";
            return false;
        }
    }

    public function error() {
        $html = "";

        $params = [
            "messages" => Container::$messages,
            "errors" => Container::$errors
        ];

        $html = self::view("error", $params);
        if ($html === false) {
            $html = "";
        }
        return $html;
    }
	
}