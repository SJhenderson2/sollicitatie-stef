<?php

class ItemModule extends Module  {
    
    public static function fetchAll() {
        $items = [];
        $groups = [];
        $items = self::fetchItems(null);
        
        $groups = GroupModule::fetchAll();
        $groups = self::fillGroupItems($groups);
        return [$items, $groups];
    }
    
    private function fillGroupItems($groups, $level=1) {
        foreach ($groups as $index => $group) {
            $groups[$index]["items"] = self::fetchItems($group["id"], $level);
            if (!empty($group["children"]) ) {
                $groups[$index]["children"] = self::fillGroupItems($groups[$index]["children"], ($level+1));
            }
        }
        return $groups;
    }
    
    public function fetchItems($parent=null, $level=0) {
        $items = [];
        if (is_null($parent) ) {
            $sql = "SELECT * FROM `item` WHERE `parent` IS NULL ORDER BY `position` ASC";
        } else {
            $sql = "SELECT * FROM `item` WHERE `parent` = :parent ORDER BY `position` ASC";
        }
        $stmt = self::$pdo->prepare($sql);
        if (!is_null($parent) ) {
            $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
        }
        if ($stmt->execute() ) {
            $items = $stmt->fetchAll();
            foreach ($items as $index => $item) {
                $items[$index]["level"] = $level;
            }
        } else {
            Container::$errors[] = "SQL ItemModule::fetchItems error";
        }
        return $items;
    }
    
    public function insertItem($name, $parent) { 
        $siblings = ItemModule::fetchItems($parent);
        $position = count($siblings);
        
        $sql = "INSERT INTO `item` (`name`, `parent`, `position`, `created`, `updated`) VALUES (:name, :parent, :position, NOW(), NOW())";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":name", $name, PDO::PARAM_STR);
        $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
        $stmt->bindValue(":position", $position, PDO::PARAM_INT);
        return $stmt->execute();
    }
    
    public function fetchItem($id) {
        $sql = "SELECT * FROM `item` WHERE `id` = :id";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        if ($stmt->execute() ) {
            $items = $stmt->fetchAll();
            if (!empty($items) ) {
                return $items[0];
            }
        }
        return false;
    }
    
    public function updateItem($id, $name, $parent, $position) { 
        $oldPosition = null;
        $oldParent = null;
        
        $item = self::fetchItem($id);
        if ($item === false) {
            Container::$errors[] = "updateItem error";
            return false;
        }

        $oldPosition = $item["position"];
        $oldParent = $item["parent"];
        $siblings = ItemModule::fetchItems($parent);
        if ($position >= count($siblings) ) {
            $position = count($siblings);
        }
        
        if ($position < $oldPosition && $parent == $oldParent) {
            if (empty($parent) ) {
                $sql = "UPDATE `item` SET `position` = `position` + 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` >= :newPosition AND `position` < :oldPosition";
            } else {
                $sql = "UPDATE `item` SET `position` = `position` + 1, `updated` = NOW() WHERE `parent` = :parent AND `position` >= :newPosition AND `position` < :oldPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($parent) ) {
                $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":newPosition", $position, PDO::PARAM_INT);
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateItem error";
                return false;
            }
        } else if ($position > $oldPosition && $parent == $oldParent) {
            if (empty($parent) ) {
                $sql = "UPDATE `item` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` > :oldPosition AND `position` < :newPosition";
            } else {
                $sql = "UPDATE `item` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` = :parent AND `position` > :oldPosition AND `position` < :newPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($parent) ) {
                $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":newPosition", $position, PDO::PARAM_INT);
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateItem error";
                return false;
            }
            $position--;
        } else if ($parent != $oldParent) {
            $position = count($siblings);
            
            if (empty($oldParent) ) {
                $sql = "UPDATE `item` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` > :oldPosition";
            } else {
                $sql = "UPDATE `item` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` = :parent AND `position` > :oldPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($oldParent) ) {
                $stmt->bindValue(":parent", $oldParent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateItem error";
                return false;
            }
        }
        $sql = "UPDATE `item` SET `name` = :name, `parent` = :parent, `position` = :position, `updated` = NOW() WHERE `id` = :id";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->bindValue(":name", $name, PDO::PARAM_STR);
        $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
        $stmt->bindValue(":position", $position, PDO::PARAM_INT);
        return $stmt->execute();
    }
    
}

?>