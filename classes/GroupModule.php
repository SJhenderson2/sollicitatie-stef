<?php

class GroupModule extends Module {
    
    public function fetchAll() {
        $groups = [];
        $sql = "SELECT * FROM `group` WHERE `parent` IS NULL ORDER BY `position` ASC";
        $stmt = self::$pdo->prepare($sql);
        if ($stmt->execute() ) {
            $groups = $stmt->fetchAll();
            foreach ($groups as $index => $group) {
                $groups[$index]["children"] = self::fetchChildren($group["id"]);
                $groups[$index]["level"] = 0;
            }
        } else {
            Container::$errors[] = "SQL GroupModule::fetchAll error";
        }
        return $groups;
    }
    
    public function fetchChildren($id, $level=1, $continue=true) {
        $groups = [];
        if (empty($id) ) {
            $sql = "SELECT * FROM `group` WHERE `parent` IS NULL ORDER BY `position` ASC";
        } else {
            $sql = "SELECT * FROM `group` WHERE `parent` = :parent ORDER BY `position` ASC";
        }
        $stmt = self::$pdo->prepare($sql);
        if (!empty($id) ) { 
            $stmt->bindValue(":parent", $id, PDO::PARAM_INT);
        }
        if ($stmt->execute() ) {
            $groups = $stmt->fetchAll();
            foreach ($groups as $index => $group) {
                if ($continue == true) {
                    $groups[$index]["children"] = self::fetchChildren($group["id"], ($level+1));
                }
                $groups[$index]["level"] = $level;
            }
        } else {
            Container::$errors[] = "SQL GroupModule::fetchChildren error";
        }
        return $groups;
    }
    
    public function insertGroup($name, $parent) { 
        $siblings = GroupModule::fetchChildren($parent);
        $position = count($siblings);
        
        $sql = "INSERT INTO `group` (`name`, `parent`, `position`, `created`, `updated`) VALUES (:name, :parent, :position, NOW(), NOW())";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":name", $name, PDO::PARAM_STR);
        $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
        $stmt->bindValue(":position", $position, PDO::PARAM_INT);
        return $stmt->execute();
    }
    
    public function fetchGroup($id) {
        $sql = "SELECT * FROM `group` WHERE `id` = :id";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        if ($stmt->execute() ) {
            $items = $stmt->fetchAll();
            if (!empty($items) ) {
                return $items[0];
            }
        }
        return false;
    }
    
    public function updateGroup($id, $name, $parent, $position) { 
        $oldPosition = null;
        $oldParent = null;
        
        $group = self::fetchGroup($id);
        if ($group === false) {
            Container::$errors[] = "updateItem error";
            return false;
        }

        $oldPosition = $group["position"];
        $oldParent = $group["parent"];
        $siblings = GroupModule::fetchChildren($parent);
        if ($position >= count($siblings) ) {
            $position = count($siblings);
        }
        if ($position < $oldPosition && $parent == $oldParent) {
            if (empty($parent) ) {
                $sql = "UPDATE `group` SET `position` = `position` + 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` >= :newPosition AND `position` < :oldPosition";
            } else {
                $sql = "UPDATE `group` SET `position` = `position` + 1, `updated` = NOW() WHERE `parent` = :parent AND `position` >= :newPosition AND `position` < :oldPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($parent) ) {
                $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":newPosition", $position, PDO::PARAM_INT);
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateGroup error";
                return false;
            }
        } else if ($position > $oldPosition && $parent == $oldParent) {
            if (empty($parent) ) {
                $sql = "UPDATE `group` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` > :oldPosition AND `position` < :newPosition";
            } else {
                $sql = "UPDATE `group` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` = :parent AND `position` > :oldPosition AND `position` < :newPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($parent) ) {
                $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":newPosition", $position, PDO::PARAM_INT);
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateGroup error";
                return false;
            }
            $position--;
        } else if ($parent != $oldParent) {
            $position = count($siblings);
            
            if (empty($oldParent) ) {
                $sql = "UPDATE `group` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` IS NULL AND `position` > :oldPosition";
            } else {
                $sql = "UPDATE `group` SET `position` = `position` - 1, `updated` = NOW() WHERE `parent` = :parent AND `position` > :oldPosition";
            }
            $stmt = self::$pdo->prepare($sql);
            if (!empty($oldParent) ) {
                $stmt->bindValue(":parent", $oldParent, PDO::PARAM_INT);
            }
            $stmt->bindValue(":oldPosition", $oldPosition, PDO::PARAM_INT);
            if (!$stmt->execute() ) {
                Container::$errors[] = "updateGroup error";
                return false;
            }
        }
        $sql = "UPDATE `group` SET `name` = :name, `parent` = :parent, `position` = :position, `updated` = NOW() WHERE `id` = :id";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->bindValue(":name", $name, PDO::PARAM_STR);
        $stmt->bindValue(":parent", $parent, PDO::PARAM_INT);
        $stmt->bindValue(":position", $position, PDO::PARAM_INT);
        return $stmt->execute();
    }
}

?>