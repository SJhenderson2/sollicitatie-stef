Beste reviewer,


Ik heb bewust voor deze opdracht gekozen om geen gebruik te maken van frameworks/libraries, omdat ik had gehoord dat er sprake was van 'inhouse' producten. 
De uitzonderingen zijn Bootstrap/FontAwesome, maar dit is alleen aan de front-end. 

Ik heb uit de opdracht de volgende onderdelen gevonden:
 - Overzicht pagina (dashboard)
 - Nieuwe groep pagina
 - Groep aanpassen/details pagina
 - Nieuw item pagina
 - Item aanpassen/details pagina
 
Een notoir gebrek is de mogelijkheid om een groep/item te verwijderen. Hiervoor zou elke module een extra pagina krijgen in verband met mogelijke recursieve verwijderingen.
Verder is er bewust geen gebruik gemaakt van autoloaders/namespaces, vanwege de tijd van 4 uur, en is er zeer zeker nog ruimte voor refactoring.
Er wordt gebruik gemaakt van een eenvoudige URL redirect met .htaccess, zodat URL's als: '/item/create' mogelijk zijn. In het bestand start.sql staat de gebruikte SQL logica, die stricter zou kunnen zijn.


Met vriendelijke groet,
Stef Henderson

=================

Opdracht:

Aan de hand van deze opdracht willen we een inschatting maken van jouw kennis en kunde van het programmeerwerk. We hebben geprobeerd een opdracht samen te stellen waar je maximaal een halve tot een hele dag werk aan zou hebben. Hieronder tref je de omschrijving, we zijn erg benieuwd naar de uitkomst en/of eventuele vragen.

We hebben twee soorten entiteiten: groepen en items. Een groep heeft een naam en kan optioneel bevat zitten in een andere groep. Een item heeft een naam en kan optioneel bevat zitten in een groep. Maak een overzicht op één pagina dat deze opzet weerspiegelt. Zorg daarnaast dat de naam van een item doorlinkt naar een achterliggende detailpagina waarop enkel de naam van het item wordt getoond.

Bied onder het overzicht enerzijds de mogelijkheid om een nieuwe groep toe te voegen, anderzijds om een nieuw item toe te voegen. Hou hierbij rekening met de hiërarchische positie waarbinnen het nieuwe object toegevoegd moet worden.

Zorg voor een rudimentaire opmaak die aansluit op de bovenstaande structuur. De opmaak moet enigszins toonbaar zijn en bruikbaar genoeg om in toekomst verder te kunnen stijlen (bijvoorbeeld door een designer).

Stuur je oplossing op als pull request in de repository https://bitbucket.org/hvmpprojects/sollicitaties.

Zeker niet onbelangrijk: hou je code simpel en overzichtelijk.