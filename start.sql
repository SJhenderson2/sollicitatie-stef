CREATE DATABASE `ernesto`;
GRANT ALL PRIVILEGES ON ernesto.* TO `ernesto`@127.0.0.1 IDENTIFIED BY 'ernesto';

CREATE TABLE `group` (
	`id` INT(11) unsigned NOT NULL auto_increment,
	PRIMARY KEY (`id`),
	`name` VARCHAR(128),
	`parent` INT(11) unsigned,
	`position` INT(3),
	`created` DATETIME,
	`updated` DATETIME,
	FOREIGN KEY (`parent`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) engine=innodb;

CREATE TABLE `item` (
	`id` INT(11) unsigned NOT NULL auto_increment,
	PRIMARY KEY (`id`),
	`name` VARCHAR(128),
	`parent` INT(11) unsigned,
	`position` INT(3),
	`created` DATETIME,
	`updated` DATETIME,
	FOREIGN KEY (`parent`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) engine=innodb;

INSERT INTO `group` (`name`, `parent`, `position`, `created`, `updated`) VALUES
 ("Test 1", NULL, 0, NOW(), NOW()), 
 ("Test 2", NULL, 1, NOW(), NOW()), 
 ("Test 3", NULL, 2, NOW(), NOW());
 INSERT INTO `group` (`name`, `parent`, `position`, `created`, `updated`) VALUES
 ("Test 1-1", 1, 0, NOW(), NOW()), 
 ("Test 1-2", 1, 1, NOW(), NOW());
 
 INSERT INTO `item` (`name`, `parent`, `position`, `created`, `updated`) VALUES
 ("Test item 1-1", 1, 0, NOW(), NOW()), 
 ("Test item 1-2", 1, 1, NOW(), NOW()),
 ("Test item 2-1", 2, 0, NOW(), NOW()),
 ("Test item 2-2", 2, 1, NOW(), NOW()),
 ("Test item 3-1", 3, 0, NOW(), NOW()),
 ("Test item 1-2-1", 5, 0, NOW(), NOW()), 
 ("Test item 1-2-2", 5, 1, NOW(), NOW()),
 ("Test item 1", NULL, 0, NOW(), NOW()), 
 ("Test item 2", NULL, 1, NOW(), NOW());